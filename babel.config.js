module.exports = {
  presets: [
    'module:metro-react-native-babel-preset',
    // We add the following code
    'module:react-native-dotenv',
    // End of added code
  ],
};
